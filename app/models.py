from app import db


class Base(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
                              onupdate=db.func.current_timestamp())


class Option(Base):
    __tablename__ = 'options'
    text = db.Column(db.String(500), nullable=True)
    selected = db.Column(db.Integer, default=0)
    # one to many relationship
    question_id = db.Column(db.Integer, db.ForeignKey('questions.id'))

    def __repr__(self):
        return '<Option {}>'.format(self.text)

    def to_dict(self):
        return {
            'id': self.id,
            'option': self.text,
            'date_created': self.date_created.strftime('%Y-%m-%d %H:%M:%S %Z'),
            'date_modified': self.date_modified,
            'question_id': self.question_id
        }


class Question(Base):
    __tablename__ = 'questions'
    text = db.Column(db.String(500), nullable=False)
    survey_id = db.Column(db.Integer, db.ForeignKey('surveys.id'))
    # many to one relationship
    options = db.relationship('Option',
                              backref=db.backref('questions', lazy=False))

    def __repr__(self):
        return '<Question {}>'.format(self.text)

    def to_dict(self):
        return {
            'id': self.id,
            'question': self.text,
            'date_created': self.date_created,
            'date_modified': self.date_modified,
            'survey_id': self.survey_id,
            'options': [option.to_dict() for option in self.options]
        }


class Survey(Base):
    __tablename__ = 'surveys'
    name = db.Column(db.String(500), nullable=False)
    # many to one relationship
    questions = db.relationship('Question',
                                backref=db.backref('surveys', lazy=False))

    def __repr__(self):
        return '<Survey {}>'.format(self.name)

    def to_dict(self):
        return {
            'id': self.id,
            'survey': self.name,
            'date_created': self.date_created,
            'date_modified': self.date_modified,
            'questions': [question.to_dict() for question in self.questions]
        }


class User(db.Model):
    __tablename__ = 'users'
    employee_id = db.Column(db.String(10), primary_key=True, nullable=True,
                            unique=True)
    username = db.Column(db.String(200), nullable=False, unique=True)
    password_hash = db.Column(db.String(128), nullable=False)
    team = db.Column(db.String(200), nullable=True)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def to_dict(self):
        return {
            'employee_id': self.employee_id,
            'username': self.username,
            'password_hash': self.password_hash,
            'team': self.team
        }
